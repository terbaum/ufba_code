import socket
import sys
import os
import threading
import pickle
import errno
import re
import time
import platform
import shutil

#__author__ = Tarbes Carvalho
# Python Version: 2.7


#definicao variaveis globais
defaultHost = '127.0.0.1'
defaultPort = 5000
buffsz = 1024
OS = platform.system()
shareFile = 'shareinfo.txt'

''' Exemplo do arquivo shareinfo.txt
# origem: compartilhador (proprietario)
# arquivo: arquivo a ser compartilhado
# destino: cliente destino (leitor)
# No cliente destino e criada uma pasta (denominada "shared") para salvar os arquivos compartilhados
# Somente esta implementada a opcao somente leitura (arquivo do proprietario sobrescreve o arquivo do leitor na pasta shared)

origem|arquivo|destino
cliente1|novoarquivo.txt|cliente2
cliente3|subpasta/documento2.html|cliente2
'''


helptext = """
Usage...
server=> sddrive.py -mode server -path ServerRootFolder -host IPAddress -port (Listen|Connect)Port
client=> sddrive.py -mode client -path ClientRootFolder -name NomeDoCliente -host IPAddress -port (Listen|Connect)Port
"""

### Funcao parseArgs()
### Objetivo: Inserir em um dicionario os parametros passados para o programa de linha de comando
### Os parametros sao tratados no formato (chave, valor)
def parseArgs():
	dict = {}
	args = sys.argv[1:]
	while len(args) >= 2:
		dict[args[0]] = args[1]
		args = args[1:]
	return dict

### Funcao CreateClientDir(clientDir)
### Objetivo: Criar a pasta do cliente (utilizando o nome do cliente) dentro da pasta de arquivos do servidor (ServerRootPath)
def CreateClientDir(clientDir):
    try:
        os.makedirs(clientDir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

### Funcao getFileList(filePath)
### Objetivo: Inserir em uma lista o nome dos arquivos em uma pasta especificada
def getFileList(filePath):
	res = []
	for root, dirs, files in os.walk(filePath):
		for fileName in files:
			relDir = os.path.relpath(root, filePath)
			if OS == 'Windows':
				fileName = re.sub("^\.\\\\","",os.path.join(relDir, fileName))
			else:
				fileName = re.sub("^\.\/","",os.path.join(relDir, fileName))
			res.append(fileName)
	return res

### Funcao: SendBytes(bytes,sock)
### Objetivo: Enviar listas serializadas para outros nos (cliente ou servidor)
def SendBytes(bytes,sock):
	i = 0
	j = buffsz

	sock.send(str(len(bytes)))
	print "Sending Bytes Lenght: " + str(len(bytes))
	while (i < len(bytes)):
		if j > (len(bytes)-1):
			j = len(bytes)
		sock.send(bytes[i:j])
		i += buffsz
		j += buffsz
### Funcao: SendFile(ClientName,ClientDirPath,filename,sock)
### Objetivo: Enviar arquivos para outros nos (cliente ou servidor)
def SendFile(ClientName,ClientDirPath,filename,sock):
	
	if OS == 'Windows':
		fileNameRelPath = ClientDirPath + "\\" + filename
	else:
		fileNameRelPath = ClientDirPath + "/" + filename
	with open(fileNameRelPath, 'rb') as f:
		bytesToSend = f.read(buffsz)
		sock.send(bytesToSend)
		while (bytesToSend):
			bytesToSend = f.read(buffsz)
			sock.send(bytesToSend)

### Funcao: ReceiveBytes(c)
### Objetivo: Receber listas serializadas de outros nos (cliente ou servidor)
def ReceiveBytes(c):
	list = ""
	Dlen = c.recv(buffsz)
	Dlen = int(Dlen)
	for i in range(0,int(Dlen/buffsz)+1):
		data = c.recv(buffsz)
		list += data
	try:
		FileList = pickle.loads(list)
		return FileList
	except:
		print "Err"
	time.sleep(0.1)

### Funcao: ReceiveFile(serverPath,clientName,filename,filesize,file_mtime,sock)
### Objetivo: Receber arquivos de outros nos (cliente ou servidor)		
def ReceiveFile(path,clientName,filename,filesize,file_mtime,sock):
	if clientName == "UpdateClientFiles":
		if OS == "Windows":
			fileNamePath = str(path) + "\\" + str(filename)
		else:
			fileNamePath = str(path) + "/" + str(filename)
	else:
		if OS == "Windows":
			fileNamePath = str(path) + "\\" + str(clientName) + "\\" + str(filename)
		else:
			fileNamePath = str(path) + "/" + str(clientName) + "/" + str(filename)
	if not os.path.exists(os.path.dirname(fileNamePath)):
		try:
			os.makedirs(os.path.dirname(fileNamePath))
		except OSError as exc:
			if exc.errno != errno.EEXIST:
				raise
	f = open(fileNamePath, 'wb')
	filesize = int(filesize)
	if filesize !=0:
		data = sock.recv(buffsz)
		totalRecv = len(data)
		f.write(data)
		while totalRecv < filesize:
			data = sock.recv(buffsz)
			totalRecv += len(data)
			f.write(data)
			print "TotalRecv: " + str(totalRecv) + " FileSize: " + str(filesize)
	f.close()
	# change file modification time to original 
	os.utime(fileNamePath, (file_mtime, file_mtime)) 
	sock.send("OK")
	time.sleep(0.1)

#### Funcao que realiza comparacoes de arquivo (se existe e data de modificacao)
def compareFileList(clientFileList, clientFileListSize, clientFileMtime, clientDirOnServer):
	ServerFileMtimeList = []
	ServerFileSizeList = []
	FileListToUpdateServer = []
	FileListToUpdateServerSize = []
	FileListToUpdateServerMtime = []
	FileListToUpdateClient = []
	FileListToUpdateClientSize = []
	FileListToUpdateClientMtime = []
	tupleFileListToUpdateServer = []
	tupleFileListToUpdateClient = []
	tuplefileListOnlyOnServer = []
	tuplefileListOnlyOnClient = []
	fileListOnlyOnClientSize = []
	fileListOnlyOnClientMtime = []
	fileListOnlyOnServerSize = []
	fileListOnlyOnServerMtime = []
	clientFileDirList = clientFileList
	fileListServerFolder = getFileList(clientDirOnServer)
	# adota uma unica convencao de nome de caminho de diretorios (pasta/subpasta) (para tratar diferencas em windows e linux)
	for i in range(0,len(fileListServerFolder)):
			fileListServerFolder[i] = re.sub("\\\\","/",fileListServerFolder[i])
	# adota uma unica convencao de nome de caminho de diretorios (pasta/subpasta) (para tratar diferencas em windows e linux)
	for i in range(0,len(clientFileDirList)):
			clientFileDirList[i] = re.sub("\\\\","/",clientFileDirList[i])
	fileListOnlyOnServer = list(set(fileListServerFolder)-set(clientFileDirList))
	fileListOnlyOnClient = list(set(clientFileDirList)-set(fileListServerFolder))
	fileListOnBoth = list(set(clientFileDirList) & set(fileListServerFolder))
	
	# ServerFileSizeList - Cria lista com o tamanho dos arquivos presentes no servidor
	for i in range(0,len(fileListServerFolder)):
		filename = fileListServerFolder[i]
		if OS == "Windows":
			fileNameRelPath = clientDirOnServer + "\\" + filename
		else:
			fileNameRelPath = clientDirOnServer + "/" + filename
		filesize = str(os.path.getsize(fileNameRelPath))
		ServerFileSizeList.append(filesize)
	
	# ServerFileMtimeList - Cria lista com o data de modificacao dos arquivos presentes no servidor
	for i in range(0,len(fileListServerFolder)):
		filename = fileListServerFolder[i]
		if OS == "Windows":
			fileNameRelPath = clientDirOnServer + "\\" + filename
		else:
			fileNameRelPath = clientDirOnServer + "/" + filename
		stat = os.stat(fileNameRelPath)
		ServerFileMtimeList.append(int(stat.st_mtime))
	
	# Cria lista com o nome dos arquivos a atualizar no servidor(FileListToUpdateServer) ou no cliente(FileListToUpdateClient)
	for i in range(0,len(fileListOnBoth)):
		FileIndexOnServerList = fileListServerFolder.index(fileListOnBoth[i])
		FileIndexOnClientList = clientFileDirList.index(fileListOnBoth[i])
		if ServerFileMtimeList[FileIndexOnServerList] > clientFileMtime[FileIndexOnClientList]:
			FileListToUpdateClient.append(fileListOnBoth[i])
		if ServerFileMtimeList[FileIndexOnServerList] < clientFileMtime[FileIndexOnClientList]:
			FileListToUpdateServer.append(fileListOnBoth[i])
	
	# tupleFileListToUpdateClient - Cria tupla com o nome, tamanho, e data de modificacao dos arquivos a atualizar no cliente (FileListToUpdateClient)
	for i in range(0,len(FileListToUpdateClient)):
		FileIndex = fileListServerFolder.index(FileListToUpdateClient[i])
		FileListToUpdateClientSize.append(ServerFileSizeList[FileIndex])
		FileListToUpdateClientMtime.append(ServerFileMtimeList[FileIndex])
	
	tupleFileListToUpdateClient.append(FileListToUpdateClient)
	tupleFileListToUpdateClient.append(FileListToUpdateClientSize)
	tupleFileListToUpdateClient.append(FileListToUpdateClientMtime)
	
	# tupleFileListToUpdateServer - Cria tupla com o nome, tamanho, e data de modificacao dos arquivos a atualizar no servidor (FileListToUpdateServer)
	for i in range(0,len(FileListToUpdateServer)):
		FileIndex = clientFileDirList.index(FileListToUpdateServer[i])
		FileListToUpdateServerSize.append(clientFileListSize[FileIndex])
		FileListToUpdateServerMtime.append(clientFileMtime[FileIndex])
	
	tupleFileListToUpdateServer.append(FileListToUpdateServer)
	tupleFileListToUpdateServer.append(FileListToUpdateServerSize)
	tupleFileListToUpdateServer.append(FileListToUpdateServerMtime)
	
	# FileListOnlyOnClient - Cria tupla com o nome, tamanho, e data de modificacao dos arquivos presentes somente no cliente
	for i in range(0,len(fileListOnlyOnClient)):
		FileIndex = clientFileDirList.index(fileListOnlyOnClient[i])
		fileListOnlyOnClientSize.append(clientFileListSize[FileIndex])
		fileListOnlyOnClientMtime.append(clientFileMtime[FileIndex])
		
	tuplefileListOnlyOnClient.append(fileListOnlyOnClient)
	tuplefileListOnlyOnClient.append(fileListOnlyOnClientSize)
	tuplefileListOnlyOnClient.append(fileListOnlyOnClientMtime)
	
	# fileListOnlyOnServer - Cria tupla com o nome, tamanho, e data de modificacao dos arquivos presentes somente no servidor
	for i in range(0,len(fileListOnlyOnServer)):
		FileIndex = fileListServerFolder.index(fileListOnlyOnServer[i])
		fileListOnlyOnServerSize.append(ServerFileSizeList[FileIndex])
		fileListOnlyOnServerMtime.append(ServerFileMtimeList[FileIndex])
		
	tuplefileListOnlyOnServer.append(fileListOnlyOnServer)
	tuplefileListOnlyOnServer.append(fileListOnlyOnServerSize)
	tuplefileListOnlyOnServer.append(fileListOnlyOnServerMtime)
	
	# Cria dicionario para retornar os dados da funcao - tuplas
	dict = {'tupleFileListToUpdateServer':tupleFileListToUpdateServer,'tupleFileListToUpdateClient':tupleFileListToUpdateClient,'tuplefileListOnlyOnServer':tuplefileListOnlyOnServer,'tuplefileListOnlyOnClient':tuplefileListOnlyOnClient}
	
	'''
	print "clientFileList: " + str(clientFileList)
	print "clientFileListSize: " + str(clientFileListSize)
	print "fileListServerFolder: " + str(fileListServerFolder)
	print "ServerFileSizeList: " + str(ServerFileSizeList)
	print "FileListToUpdateServer: " + str(FileListToUpdateServer)
	print "FileListToUpdateClient: " + str(FileListToUpdateClient)
	print "fileListOnlyOnServer: " + str(fileListOnlyOnServer)
	print "fileListOnlyOnClient: " + str(fileListOnlyOnClient)
	print "fileListOnBoth: " + str(fileListOnBoth)
	print "tupleFileListToUpdateServer: " + str(tupleFileListToUpdateServer)
	print "tupleFileListToUpdateClient: " + str(tupleFileListToUpdateClient)
	print "tuplefileListOnlyOnServer: " + str(tuplefileListOnlyOnServer)
	print "tuplefileListOnlyOnClient: " + str(tuplefileListOnlyOnClient)
	'''
	return dict

#### Funcao que realiza o compartilhamento de arquivos entre um cliente e outro (SOMENTE_LEITURA)
def ShareFiles(serverPath):
	if os.path.isfile(shareFile):
		shareinfo = open(shareFile).readlines()[1:]
		for line in shareinfo:
			origem,arquivo,destino = line.split('|')
			if OS == "Windows":
				origem = serverPath+"\\"+origem
				destino = serverPath+"\\"+destino
				arquivoPath = origem+"\\"+arquivo
				dFolder = destino+"\\"+"shared"
				dFile = dFolder+"\\"+arquivo
			else:
				origem = serverPath+"/"+origem
				destino = serverPath+"/"+destino
				arquivoPath = origem+"/"+arquivo
				dFolder = destino+"/"+"shared"
				dFile = dFolder+"/"+arquivo
			if os.path.isdir(origem):
				if os.path.isdir(destino):
					if os.path.isfile(arquivoPath):
						CreateClientDir(dFolder)
						try:
							shutil.copy2(arquivoPath, dFile)
						except shutil.Error as e:
							print('Error: %s' % e)
					
#### Funcao que realiza a atualizacao de arquivos no servidor
def UpdateServer(path,clientName,lista,s):
		data = pickle.dumps(lista[0])
		SendBytes(data,s) #Envia lista de arquivos
		
		FileQty = len(lista[0])
		
		for i in range(0,int(FileQty)):
			filename = lista[0][i]
			filesize = lista[1][i]
			file_mtime = lista[2][i]
			ReceiveFile(path,clientName,filename,filesize,file_mtime,s)

#### Funcao que realiza a atualizacao de arquivos no servidor
def UpdateClient(path,clientName,lista,s):
		data = pickle.dumps(lista)
		SendBytes(data,s) #Envia lista de arquivos
		
		FileQty = len(lista[0])
		for i in range(0,int(FileQty)):
			filename = lista[0][i]
			SendFile(clientName,path,filename,s)
			if (s.recv(buffsz)) == "OK":
				continue
		
### Funcao: run(c, addr, serverPath)
### Objetivo: Funcao principal do Server para tratar do envio e recebimento de arquivos
def run(c, addr, serverPath):
	
	print 'Connection from: ' + str(addr)
	print "Receiving data From: " + str(addr)
	
	info = ReceiveBytes(c)	#Recebe lista de parametros necessarios
	clientName = info[0]
	fileQty = info[1]
	clientFileList = info[2]
	clientFileListSize = info[3]
	clientFileMtime = info[4]
	if OS == "Windows":
		clientDir = serverPath+"\\"+clientName
	else:
		clientDir = serverPath+"/"+clientName
	CreateClientDir(clientDir)
	Lists = compareFileList(clientFileList, clientFileListSize, clientFileMtime, clientDir)
	
	if Lists['tuplefileListOnlyOnServer'][0]:
		print "IDENTIFICADO EM %%%%%%%%%%% tuplefileListOnlyOnServer %%%%%%%%%"
		print(Lists['tuplefileListOnlyOnServer'])
		c.send("UPDATE_CLIENT")
		UpdateClient(clientDir,clientName,Lists['tuplefileListOnlyOnServer'],c)
		
	elif Lists['tuplefileListOnlyOnClient'][0]:
		print "IDENTIFICADO EM %%%%%%%%%%% tuplefileListOnlyOnClient %%%%%%%%%"
		c.send("UPDATE_SERVER")
		UpdateServer(serverPath,clientName,Lists['tuplefileListOnlyOnClient'],c)
		
	elif Lists['tupleFileListToUpdateServer'][0]:
		print "IDENTIFICADO EM %%%%%%%%%%% tupleFileListToUpdateServer %%%%%%%%%"
		c.send("UPDATE_SERVER")
		UpdateServer(serverPath,clientName,Lists['tupleFileListToUpdateServer'],c)
		
	elif Lists['tupleFileListToUpdateClient'][0]:
		print "IDENTIFICADO EM %%%%%%%%%%% tupleFileListToUpdateClient %%%%%%%%%"
		c.send("UPDATE_CLIENT")
		UpdateClient(clientDir,clientName,Lists['tupleFileListToUpdateClient'],c)

	print 'Closing client connection', addr
	ShareFiles(serverPath)
	c.close()

### Funcao: Client(clientName,dirPath)
### Objetivo: Chamada da funcao Client quando eh selecionada a opcao (-mode client)
def Client(host,port,clientName,dirPath):
	
	while True:
		s = socket.socket()
		s.connect((host,port))
		print "Nome do cliente: " + str(clientName) 
		
		
		info = []
		fileSizeList = []
		fileMtimeList = []
		fileList = getFileList(dirPath)
		fileQty = len(fileList)
		info.append(str(clientName)) # Posicao 0 da lista
		info.append(str(fileQty))    # Posicao 1 da lista
		info.append(fileList)		 # Posicao 2 da lista (matriz)
		
		# Insere array com o tamanho dos arquivos
		for i in range(0,len(fileList)):
			filename = fileList[i]
			if OS == "Windows":
				fileNameRelPath = dirPath + "\\" + filename
			else:
				fileNameRelPath = dirPath + "/" + filename
			filesize = str(os.path.getsize(fileNameRelPath))
			fileSizeList.append(filesize)
		
		#Insere array com a data de modificacao do arquivo
		for i in range(0,len(fileList)):
			filename = fileList[i]
			if OS == "Windows":
				fileNameRelPath = dirPath + "\\" + filename
			else:
				fileNameRelPath = dirPath + "/" + filename
			stat = os.stat(fileNameRelPath)
			fileMtimeList.append(int(stat.st_mtime))
		
		info.append(fileSizeList)  			# Posicao 3 da lista
		info.append(fileMtimeList)  		# Posicao 4 da lista
		data = pickle.dumps(info)
		SendBytes(data,s) #Envia lista de parametros necessarios
		
		updateType = s.recv(buffsz)
		if (updateType) == "UPDATE_SERVER":
			fileListToSend = ReceiveBytes(s)
			for i in range(0,len(fileListToSend)):
				filename = fileListToSend[i]
				SendFile(clientName,dirPath,filename,s)
				if (s.recv(buffsz)) == "OK":
					continue
					
		if (updateType) == "UPDATE_CLIENT":
			fileListToReceive = ReceiveBytes(s)
			FileQty = len(fileListToReceive[0])
			
			for i in range(0,int(FileQty)):
				filename = fileListToReceive[0][i]
				filesize = fileListToReceive[1][i]
				file_mtime = fileListToReceive[2][i]
				ReceiveFile(dirPath,"UpdateClientFiles",filename,filesize,file_mtime,s)
		s.close()	
		time.sleep(10)

### Funcao: Server(serverPath)
### Objetivo: Chamada da funcao Server quando eh selecionada a opcao (-mode server)
def Server(host,port,serverPath):
	s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.bind((host,port))
	s.listen(5)
	print "Starting server on address: " + str(host) + " and port " + str(port)
	while True:
		c, addr = s.accept()
		t = threading.Thread(target=run,args=tuple([c, addr, serverPath]))
		t.start()
	
### Funcao: Main(args)
### Objetivo: Funcao principal do programa que trata do direcionamento dos modos de funcionamento (server e client)
def Main(args):
	host = args.get('-host', defaultHost)			# use args or defaults
	port = int(args.get('-port', defaultPort))
	if args.get('-mode') == 'server':
		if args.get('-path'):
			Server(host,port,args['-path'])
		else:
			print(helptext)
	elif args.get('-mode') == 'client':
		if args.get('-path'):
			if args.get('-name'):
				Client(host,port,args['-name'],args['-path'])
			else:
				print(helptext)
		else:
			print(helptext)
	else:
		print(helptext)	
	
if __name__ == '__main__':
	args = parseArgs()
	Main(args)